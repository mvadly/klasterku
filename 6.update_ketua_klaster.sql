UPDATE clusters c SET
c.head_cluster_member = IFNULL((SELECT id FROM members WHERE cluster_id = c.id AND flag_head=1 LIMIT 1),0),
c.`rpt_total_member` = (SELECT COUNT(*) FROM members WHERE cluster_id = c.id);
