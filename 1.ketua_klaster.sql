USE klasterkunew_migrasi;
TRUNCATE members_migrate;
INSERT INTO members_migrate (
cluster_id, `name`, nik,

sex, -- switch case

birthplace, hp, loan_norek_member, save_norek_member,

loan_status_member, -- switch case

save_status_member, -- switch case

created_at, created_by, flag_head
)

SELECT 
o.id, IFNULL(o.kelompok_perwakilan, ""), IFNULL(o.kelompok_NIK, ""),

CASE LOWER(o.kelompok_jenis_kelamin)
WHEN "pria" THEN "L"
WHEN "wanita" THEN "P"
ELSE "" END AS sex,

o.kelompok_perwakilan_tempat_lahir, o.kelompok_handphone, IFNULL(o.norek_pinjaman_bri, ""), "",

IFNULL(o.pinjaman, ""),

IFNULL(o.simpanan_bank, ""),

o.`timestamp`,RIGHT(IFNULL(o.personal_number, ""), 8) AS created_by, 1


FROM klasterku_migrasi.migrate_cluster o;
