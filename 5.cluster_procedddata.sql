DESC cluster_processeddata;
DESC klasterku_migrasi.migrate_cluster_processeddata;

INSERT INTO klasterkunew_migrasi.cluster_processeddata(
id_data,
kode_uker,
id_cluster,
id_kategori_usaha,
nama_kategori_usaha,
total_anggota,
agen_brilink,
trx_agen_brilink,
nominal_fbi,
total_rekening_simpanan,
total_saldo_simpanan,
total_rekening_pinjaman,
total_saldo_pinjaman,
kolektabilitas,
periode_data,
created_at,
created_by,
updated_at,
updated_by)

SELECT 
id_data,
kode_uker,
new_cluster_id,
id_kategori_usaha,
nama_kategori_usaha,
total_anggota,
agen_brilink,
trx_agen_brilink,
nominal_fbi,
total_rekening_simpanan,
total_saldo_simpanan,
total_rekening_pinjaman,
total_saldo_pinjaman,
kolektabilitas,
periode_data,
created_at,
created_by,
updated_at,
updated_by

FROM klasterku_migrasi.migrate_cluster_processeddata;
